exports.config = {
  framework: 'jasmine',
  seleniumAddress: 'http://localhost:4444/wd/hub',
  specs: ['trello-for-netguru-spec.js'],
  capabilities: {
    browserName: 'chrome',
    chromeOptions: {
      // How to set browser language (menus & so on)
      args: [ 'lang=en-GB' ],
      // How to set Accept-Language header
      prefs: {
        intl: { accept_languages: "en-GB" }
      }
    }
  }
}