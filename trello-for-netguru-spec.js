const EC = protractor.ExpectedConditions;

describe('My user on Trello.com', function() {

  function logout() {
    const menuPopup = element(by.css(".header-btn.header-avatar.js-open-header-member-menu"));
    menuPopup.click();
    element(by.css(".js-logout")).click();
  }

  beforeEach(function() {
    browser.waitForAngularEnabled(false); // because Trello is not an Angular app
    browser.get('https://trello.com/');
    element(by.css(".btn.btn-sm.btn-link.text-white")).click();
  });

  it('should log in successfully', function() {
    element(by.id("user")).sendKeys("adam.piasecki200@gmail.com"); 
    // Usually here you would like to use a generated email, but it's fine as it is now for the recruitment process
    element(by.id("password")).sendKeys("Abcd1234");
    element(by.id("login")).click();

    browser.wait(EC.urlContains("https://trello.com/adam56273754/boards"), 5000);

    const allBoardsElement = element(by.css(".content-all-boards"));
    expect(allBoardsElement.isPresent()).toBe(true);
    logout();
  });
  
  it('should log in with an incorect password', function() {
    element(by.id("user")).sendKeys("adam.piasecki200@gmail.com"); // correct email
    element(by.id("password")).sendKeys("Abcd5678"); // using incorect password
    element(by.id("login")).click();

    browser.wait(EC.textToBePresentInElement($('p.error-message'), "Invalid password"), 5000);
    expect(true).toBe(true); // Previous line works, so it's all good!
  });
});