# Trello tests

- To run, you will need to have *Node.js* installed. You will download Protractor package using npm, which comes with Node.js. 
- Check the version of Node.js you have by running 
```
node --version
```
 - Keep in mind that Protractor 5 is compatible with nodejs v6 and newer.

- You will need to have the Java Development Kit (JDK) installed to run the standalone Selenium Server. Check this by running java -version from the command line.

- Use npm to install Protractor globally with:
```
npm install -g protractor
```
- This will install two command line tools, protractor and webdriver-manager. Try running protractor --version to make sure it's working.
The webdriver-manager is a helper tool to easily get an instance of a Selenium Server running. Use it to download the necessary binaries with:

```
webdriver-manager update
```

- Now start up a server with:

```
webdriver-manager start
```

- This will start up a Selenium Server and will output a bunch of info logs. Our tests will send requests to this server to control a local browser. Leave this server running throughout the testing. You can see information about the status of the server at http://localhost:4444/wd/hub.

- Open a new command line or terminal window and type:
```
protractor conf.js
```

The test output should be 2 specs, 0 failures.